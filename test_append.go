package main

import (
    "fmt"
    "sort"
)

func sortNums(intSlice []int) {
  sort.Ints(intSlice)
  fmt.Printf("did sort work? %v\n", intSlice)
  appendANum(intSlice)

}

func appendANum(intSlice []int){
  intSlice = append(intSlice, 5)
  fmt.Printf("did append work? %v\n", intSlice)
}

func main() {
  someNums := []int{4,3,2,1}

  fmt.Printf("some numbers: %v\n", someNums)
  sortNums(someNums)
  fmt.Printf("some numbers after returning to main from sortNums: %v\n", someNums)

}
