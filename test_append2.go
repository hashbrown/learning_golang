package main

import (
    "fmt"
    "sort"
)

func sortNums(intSlice []int) {
  sort.Ints(intSlice)
  fmt.Printf("did sort work? %v\n", intSlice)
  appendANum(intSlice)

}

func appendANum(intSlice []int) []int{
  intSlice = append(intSlice, 5)
  return intSlice
}

func appendAlot(intSlice []int, numAppends int) []int {
    for i := 0; i < numAppends; i++ {
	intSlice = append(intSlice, i)
    }
    return intSlice
}

func main() {

  fmt.Println("creating a slice length and capacity 0:")
  var moreNums []int
  fmt.Printf(" moreNums length: %v\n moreNums capacity: %v\n\n", len(moreNums), cap(moreNums))

  fmt.Println("appending 1 number to the slice")
  moreNums = appendANum(moreNums)
  fmt.Printf(" moreNums length: %v\n moreNums capacity: %v\n\n", len(moreNums), cap(moreNums))

  fmt.Println("appending 1 number to the slice")
  moreNums = appendANum(moreNums)
  fmt.Printf(" moreNums length: %v\n moreNums capacity: %v\n\n", len(moreNums), cap(moreNums))

  fmt.Println("appending 1 number to the slice")
  moreNums = appendANum(moreNums)
  fmt.Printf(" moreNums length: %v\n moreNums capacity: %v\n\n", len(moreNums), cap(moreNums))

  fmt.Println("appending 1 number to the slice")
  moreNums = appendANum(moreNums)
  fmt.Printf(" moreNums length: %v\n moreNums capacity: %v\n\n", len(moreNums), cap(moreNums))

  fmt.Println("appending 1 number to the slice")
  moreNums = appendANum(moreNums)
  fmt.Printf(" moreNums length: %v\n moreNums capacity: %v\n\n", len(moreNums), cap(moreNums))

  fmt.Println("appending 5 numbers to the slice")
  moreNums = appendAlot(moreNums, 5)
  fmt.Printf(" moreNums length: %v\n moreNums capacity: %v\n\n", len(moreNums), cap(moreNums))

  fmt.Println("appending 7 numbers to the slice")
  moreNums = appendAlot(moreNums, 7)
  fmt.Printf(" moreNums length: %v\n moreNums capacity: %v\n\n", len(moreNums), cap(moreNums))

  fmt.Println("appending 16 numbers to the slice")
  moreNums = appendAlot(moreNums, 16)
  fmt.Printf(" moreNums length: %v\n moreNums capacity: %v\n\n", len(moreNums), cap(moreNums))

  fmt.Println("appending 1024 numbers to the slice")
  moreNums = appendAlot(moreNums, 1024)
  fmt.Printf(" moreNums length: %v\n moreNums capacity: %v\n\n", len(moreNums), cap(moreNums))

}
